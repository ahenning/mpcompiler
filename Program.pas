program sample3;
  var v:   Integer;
  procedure count( var v: Integer; n: integer; var sum: Integer );
    var i: integer;  { local variables }
  begin
    i := 1;
    sum := 0.0;
    while ( i <= n )  do { set-up the loop }
    begin
      sum := sum + v;
      i := i + 1;
    end;
  end;
begin
  v := 11;
  v := 22.2e3;
  v := 33.3e-1;
  v := 44.9;
  write(sum);       { write result }
end.