app: mp.o Scanner.o MPParser.o
	g++ -o mp mp.o Scanner.o MPParser.o

mp.o: mp.cpp Scanner.h
	g++ -c mp.cpp

Scanner.o: Scanner.cpp Scanner.h
	g++ -c Scanner.cpp
	
MPParser.o: MPParser.cpp MPParser.h
	g++ -c MPParser.cpp
