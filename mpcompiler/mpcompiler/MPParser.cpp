
#include "MPParser.h"
#include <queue>

using namespace std;

bool prep = true;
bool foundFirstLocalEntry = false;

tokenLexeme newTokenData;
tokenLexeme tokenDataLookAhead;
ifstream fileOfTokens;
int tempOffset = 0;
queue<string> identifierQueue;
queue<tokenLexeme> exp_rec;
string id_rec;
string currentLexeme;


MPParser::MPParser(void)
{
	fileOfTokens.open ("outFile.txt");
	root = new symbolTable;
	root->nextSymbol = NULL;
	root->nextTable = NULL;
	root = NULL;
	pushNext = false;
	varInitCount = 0;
	regNum = 0;
	err = false;
}

void MPParser::clearExpRec(void)
{
	while(!exp_rec.empty())
	{
		exp_rec.pop();
	}
}

void MPParser::clearIDQ(void)
{
	while(!identifierQueue.empty())
	{
		identifierQueue.pop();
	}
}
string MPParser::getExpType(void)
{
	queue<tokenLexeme> tempQueue = exp_rec;
	string expToken;
	if(exp_rec.size() != 0)
	{
		expToken = exp_rec.front().token;
		if(strcmp(expToken.c_str(), "MP_LPAREN") == 0)
		{
			
			while(strcmp(tempQueue.front().token.c_str(), "MP_LPAREN")  == 0)
			{
				tempQueue.pop();
			}
				expToken = tempQueue.front().token;
		}
	}
	else
	{
		cout << "exp_rec is empty" << endl;
	}
	string retType;
	if(expToken.compare("MP_IDENTIFIER")==0)
	{
		if(tempQueue.size() != 0)
		{
	 		symbolTable *tableSearch = new symbolTable;
			tableSearch = an.findInSymbolTable(tempQueue.front().lexeme);
			if(tableSearch != NULL)
			{
				retType = tableSearch->type;
			}
			else
			{
				cout << "Variable Not Found: " << tempQueue.front().lexeme << endl;
			}
		}
	}else if(expToken.compare("MP_INTEGER_LIT")==0)
	{
		retType = "Integer";
	}else if(expToken.compare("MP_FLOAT_LIT")==0)
	{
		retType = "Float";
	}else if(expToken.compare("MP_FIXED_LIT")==0)
	{
		retType = "Fixed";
	}else if(expToken.compare("MP_STRING_LIT")==0)
	{
		retType = "String";
	}
	return retType;
}
void MPParser::getToken(void)
{
  tokenDataLookAhead.token.clear();
  tokenDataLookAhead.lexeme.clear();
  tokenDataLookAhead.row = 0;
  tokenDataLookAhead.column = 0;

	string data;
	if(fileOfTokens.is_open())
	{
			char character[1] = {fileOfTokens.peek()};
			
	        	
			while(character[0] != ' ' && character[0] != 9 && character[0] != 10 && character[0] != 13 && !fileOfTokens.eof()){
				character[0] = fileOfTokens.peek();
				if(character[0] == -1)
					break;
				if(fileOfTokens.peek() != 9)
				{
				  tokenDataLookAhead.token.append(1, (char)fileOfTokens.get());
				}
			}

			fileOfTokens.get();
			character[0] = fileOfTokens.peek();
			while(isspace(character[0]) || character[0] == 9 || character[0] == 10 || character[0] == 13){
				character[0] = fileOfTokens.peek();
				fileOfTokens.get();
			}

			character[0] = fileOfTokens.peek();
			while(character[0] != ' ' && character[0] != 9 && character[0] != 10 && character[0] != 13 && !fileOfTokens.eof()){
				character[0] = fileOfTokens.peek();
				if(character[0] == -1)
					break;
				data.append(1, (char)fileOfTokens.get()); 
				tokenDataLookAhead.row = atoi(data.c_str());
			}

			data.clear();

			character[0] = fileOfTokens.peek();
			while(isspace(character[0]) || character[0] == 9 || character[0] == 10 || character[0] == 13){
				character[0] = fileOfTokens.peek();
				fileOfTokens.get();
			}

			character[0] = fileOfTokens.peek();
			while(character[0] != ' ' && character[0] != 9 && character[0] != 10 && character[0] != 13 && !fileOfTokens.eof()){
				character[0] = fileOfTokens.peek();
				if(character[0] == -1)
					break;
				data.append(1, (char)fileOfTokens.get()); 
				tokenDataLookAhead.column = atoi(data.c_str());
			}

			data.clear();

			character[0] = fileOfTokens.peek();
			while(isspace(character[0]) || character[0] == 9 || character[0] == 10 || character[0] == 13){
				character[0] = fileOfTokens.peek();
				if(isspace(character[0]) || character[0] == 9 || character[0] == 10 || character[0] == 13)
				{
					fileOfTokens.get();
				}
			}

			character[0] = fileOfTokens.peek();
			if(tokenDataLookAhead.token.compare("MP_STRING_LIT") == 0)
			{
				while(character[0] != 10 && character[0] != 13 && character[0] != ';')
				{
					if(character[0] == -1)
						break;
					tokenDataLookAhead.lexeme.append(1, (char)fileOfTokens.get());
					character[0] = fileOfTokens.peek();
				}
				fileOfTokens.get();
			}
			else
			{
				while(character[0] != ' ' && character[0] != 9 && character[0] != 10 && character[0] != 13 && !fileOfTokens.eof() && character[0] != '\n'){
					character[0] = fileOfTokens.peek();
					if(character[0] == -1)
						break;
					tokenDataLookAhead.lexeme.append(1, (char)fileOfTokens.get());
					character[0] = fileOfTokens.peek();
				}
			}

			character[0] = fileOfTokens.peek();
			while(isspace(character[0]) || character[0] == 9 || character[0] == 10 || character[0] == 13){
				fileOfTokens.get();
				character[0] = fileOfTokens.peek();
			}
			
                      
			newTokenData.token = tokenDataLookAhead.token;
			newTokenData.row = tokenDataLookAhead.row;
			newTokenData.column = tokenDataLookAhead.column;
			newTokenData.lexeme = tokenDataLookAhead.lexeme;

	}
	else
	{
		cout << "404 - - File Not Found!" << endl;
	}
	
}

void MPParser::printOutSymbolTables(void)
{

}

//
void MPParser::insertToSymbolTable(string identifier, string type, string kind)
{
	if(strcmp(kind.c_str(), "Program") != 0)
	{
		tempOffset++;
	}
	symbolTable *tempNode = new symbolTable;
	tempNode->kind = kind;
	tempNode->type = type;
	tempNode->lexeme = identifier;
	tempNode->offSet = tempOffset;
	if(root != NULL)
	{
		tempNode->nextTable = root->nextTable;
	}
	tempNode->nextSymbol = root;
	if(root != NULL)
	{
		root->nextTable = NULL;
	}
	else
	{
		tempNode->nextTable = NULL;
	}
	root = tempNode;
	an.assignTable(root);
}

void MPParser::pushSymbolTable(string identifier, string type, string kind)
{
	tempOffset = 0;
	regNum++;
	an.setRegNum(regNum);
	an.assignTable(root);
	symbolTable *tempNode = new symbolTable;
	tempNode->kind = kind;
	tempNode->type = type;
	tempNode->lexeme = identifier;
	tempNode->offSet = tempOffset;
	tempNode->nextSymbol = NULL;
	tempNode->nextTable = root;
	root = tempNode;
	pushNext = false;
	an.assignTable(root);
}


void MPParser::popSymbolTable()
{
	regNum--;
	an.setRegNum(regNum);
	symbolTable *temp = new symbolTable;                 
	temp = root;										
	if(temp != NULL)
	{
      root = temp->nextTable;						
    }
	an.assignTable(root);

}


void MPParser::error(void)
{
	cout << "some error at row:" << newTokenData.row;
	cout << " column:" << newTokenData.column << " " << newTokenData.token << "  " << newTokenData.lexeme;
	err = true;
}

void MPParser::parseIt()
{
	
  getToken();
  lookAheadNumber = getLookaheadNumber(newTokenData.token);
	switch(lookAheadNumber)
	{
		case 18://program
			systemGoal();
			break;
		default:
			//cout << "error in parseThatShit()" << endl;
		    error();
			break;
	}
}


void MPParser::match(string matchToken)
{
	currentLexeme = newTokenData.lexeme;
	if(matchToken.compare("MP_BEGIN") == 0 && !foundFirstLocalEntry)
	{
		foundFirstLocalEntry = false;
	}
	if(matchToken.compare("MP_EMPTY") == 0)
	{
		//do nothing for right now?
	}
	else if(matchToken.compare("MP_EOF") == 0)
	{
		cout << "The input program parses!";
	}
	else if(matchToken.compare(newTokenData.token) == 0)
	{
		if(newTokenData.token.compare("MP_STRING_LIT"))
		{
			parseFile << newTokenData.token << "\t\t";
			parseFile << newTokenData.lexeme << endl;
		}
		else
		{
			parseFile << newTokenData.token << "\t\t";
			parseFile << newTokenData.lexeme;
		}
		getToken();
		lookAheadNumber = getLookaheadNumber(newTokenData.token);
	}
	else
	{ 
		syntaxError(matchToken);
	}
}

void MPParser::syntaxError(string matchToken)
{
	err = true;
	string response;
	cout << "Syntax Error: Expected " << matchToken << " instead found " <<  newTokenData.token << " at line: " << newTokenData.row << " and column: "  << newTokenData.column << ".\n";
	cout << "Press Enter to ignore error and continue parsing...";
	cin.ignore(1);
	getToken();
	lookAheadNumber = getLookaheadNumber(newTokenData.token);
	
}

int MPParser::getLookaheadNumber(string find)
{
	string lookTable[54];
	lookTable[0] = "MP_AND";
	lookTable[1] = "MP_BEGIN";
	lookTable[2] = "MP_DIV";
	lookTable[3] = "MP_DO";
	lookTable[4] = "MP_DOWNTO";
	lookTable[5] = "MP_ELSE";
	lookTable[6] = "MP_END";
	lookTable[7] = "MP_FIXED";
	lookTable[8] = "MP_FLOAT";
	lookTable[9] = "MP_FOR";
	lookTable[10] = "MP_FUNCTION";
	lookTable[11] = "MP_IF";
	lookTable[12] = "MP_INTEGER";
	lookTable[13] = "MP_MOD";
	lookTable[14] = "MP_NOT";
	lookTable[15] = "MP_OR";
	lookTable[16] = "MP_PROCEDURE";
	lookTable[17] = "MP_PROGRAM";
	lookTable[18] = "MP_READ";
	lookTable[19] = "MP_REPEAT";
	lookTable[20] = "MP_THEN";
	lookTable[21] = "MP_TO";
	lookTable[22] = "MP_UNTIL";
	lookTable[23] = "MP_VAR";
	lookTable[24] = "MP_WHILE";
	lookTable[25] = "MP_WRITE";
	lookTable[26] = "MP_IDENTIFIER";
	lookTable[27] = "MP_INTEGER_LIT";
	lookTable[28] = "MP_FIXED_LIT";
	lookTable[29] = "MP_FLOAT_LIT";
	lookTable[30] = "MP_STRING_LIT";
	lookTable[31] = "MP_PERIOD";
	lookTable[32] = "MP_COMMA";
	lookTable[33] = "MP_SCOLON";
	lookTable[34] = "MP_LPAREN";
	lookTable[35] = "MP_RPAREN";
	lookTable[36] = "MP_EQUAL";
	lookTable[37] = "MP_GTHAN";
	lookTable[38] = "MP_GEQUAL";
	lookTable[39] = "MP_LTHAN";
	lookTable[40] = "MP_LEQUAL";
	lookTable[41] = "MP_NEQUAL";
	lookTable[42] = "MP_ASSIGN";
	lookTable[43] = "MP_PLUS";
	lookTable[44] = "MP_MINUS";
	lookTable[45] = "MP_TIMES";
	lookTable[46] = "MP_COLON";
	lookTable[47] = "MP_EOF";
	lookTable[48] = "MP_BOOLEAN";
	lookTable[49] = "MP_WRITELN";
	lookTable[50] = "MP_DIVIDE";
	lookTable[51] = "MP_TRUE";
	lookTable[52] = "MP_FALSE";
	lookTable[53] = "MP_STRING";
	string temp; 
	temp = find;
        
	for(int i = 0; i < 54; i++)
	{
	  
		if(temp.compare(lookTable[i]) == 0)
		{
			return i + 1;
		}
	}
	
	return 55;
	
}

// systemGoal, program, programHeading and block methods
void MPParser::systemGoal(void)
{
	program();
	match("MP_EOF");
}

void MPParser::program(void)
{
  programHeading();
  match("MP_SCOLON");
  block();
  popSymbolTable();
  match("MP_PERIOD");
	if(!err && !an.getErr()){an.genHalt();}
}

void MPParser::programHeading(void)
{
	
  match("MP_PROGRAM");
  programIdentifier();
}

void MPParser::block(void)
{
  
	switch(lookAheadNumber)
	{
		case 24://var
			variableDeclarationPart();
			if(!err && !an.getErr()){an.initVars(varInitCount, regNum);}
			varInitCount = 0;
			procedureAndFunctionDeclarationPart();
			statementPart();
			break;
		case 17: //procedure
			variableDeclarationPart();
			if(!err && !an.getErr()){an.initVars(varInitCount, regNum);}
			varInitCount = 0;
           		procedureAndFunctionDeclarationPart();
            		statementPart();
			break;
		case 11: //function
			variableDeclarationPart();
			if(!err && !an.getErr()){an.initVars(varInitCount, regNum);}
			varInitCount = 0;
            		procedureAndFunctionDeclarationPart();
            		statementPart();
			break;
		case 2: //begin
			variableDeclarationPart();
			if(!err && !an.getErr()){an.initVars(varInitCount, regNum);}
			varInitCount = 0;
            		procedureAndFunctionDeclarationPart();
            		statementPart();
			break;
		default:
			//cout << "error in block line 299" << endl;
			error();
			break;
		
	}
}
  
// methods variableDeclarationPart, variableDeclarationTail
// variableDeclaration, type
void MPParser::variableDeclarationPart()
{
	switch(lookAheadNumber)
	{	
		case 24: //var
			match("MP_VAR");
			variableDeclaration();
			match("MP_SCOLON");
			variableDeclarationTail();
			break;
		case 11: //function
			match("MP_EMPTY");
			break;		
		case 17: //procedure
			match("MP_EMPTY");
			break;
		case 2: //begin
			match("MP_EMPTY");
			break;
		case 34: //semi-colon
			match("MP_EMPTY");
			break;
		default: //error case
			//cout << "error in variableDeclarationPart line 333" << endl;
			error();
			break;
	}
}

void MPParser::variableDeclarationTail(void)
{
	switch(lookAheadNumber)
	{
		case 27: // identifier
			variableDeclaration();
			match("MP_SCOLON");
			variableDeclarationTail();
			break;
		case 11://function
			match("MP_EMPTY");
			break;
		case 17://procedure
			match("MP_EMPTY");
			break;
		case 2: //begin
			match("MP_EMPTY");
			break;
		default: //error case
			//cout << "error in variableDeclarationTail line 362" << endl;
			error();
			break;
	}	
	
}

void MPParser::variableDeclaration(void)
{
	switch(lookAheadNumber)
	{
		case 27: // identifier
			identifierList();
			match("MP_COLON");
			type();
			break;
		default: //error case
			//cout << "error in variableDeclaration line 388" << endl;
			error();
			break;
	}	
	
}

void MPParser::type(void)
{
	switch(lookAheadNumber)
	{
		case 13: //integer
		  while(identifierQueue.size() != 0)
		    {
				insertToSymbolTable(identifierQueue.front(), "Integer", "Var");
				varInitCount++;
				identifierQueue.pop();
		    }			
			match("MP_INTEGER");
			break;
		case 9: //float
		  while(identifierQueue.size() !=0)
		  {
			insertToSymbolTable(identifierQueue.front(), "Float", "Var");
			varInitCount++;
		    identifierQueue.pop();
		  }
		  match("MP_FLOAT");
			break;
		case 49: //boolean
		  while(identifierQueue.size() !=0)
		  {
			insertToSymbolTable(identifierQueue.front(), "Boolean", "Var");
			varInitCount++;
		    identifierQueue.pop();
		  }
		  match("MP_BOOLEAN");
			
			break;
		case 8: //fixed
		  while(identifierQueue.size() !=0)
		    {
				insertToSymbolTable(identifierQueue.front(), "Fixed", "Var");
				varInitCount++;
				identifierQueue.pop();
		    }
		  match("MP_FIXED");
		  break;
		case 54: //string
			 while(identifierQueue.size() !=0)
		   	 {
					insertToSymbolTable(identifierQueue.front(), "String", "Var");
					varInitCount++;
		      		identifierQueue.pop();
		    }
			match("MP_STRING");
			break;
		default: //error case
			//cout << "error in type line 405" << endl;
			error();
			break;
	}	
}


//next section. methods: procedureAndFunctionDeclarationPart, procedureDeclaration functionDeclaration
// procedureHeading, functionHeading, optionalFormalParameterList,formalParameterSectionTail, formalParameterSection,
//valueParameterSection, variableParameterSection
  void MPParser::procedureAndFunctionDeclarationPart(void)
  {
    switch(lookAheadNumber)
    {
	case 17://procedure
		procedureDeclaration();
		procedureAndFunctionDeclarationPart();
		break;
    case 11://function
    	functionDeclaration();
		procedureAndFunctionDeclarationPart();
		break;
    case 2://begin
    	match("MP_EMPTY");
		break;
    default:
		//cout << "error in procedureAndFunctionDeclarationPart line 436" << endl;
		error();
		break;
	}
}

void MPParser::procedureDeclaration(void)
{
	procedureHeading();
	match("MP_SCOLON");
	block();
	popSymbolTable();
	match("MP_SCOLON");
}

void MPParser::functionDeclaration(void)
{
	functionHeading();
	match("MP_SCOLON");
	block();
	popSymbolTable();
	match("MP_SCOLON");
}

void MPParser::procedureHeading(void)
{
	match("MP_PROCEDURE");
	procedureIdentifier();
	optionalFormalParameterList();
}

void MPParser::functionHeading(void)
{
	match("MP_FUNCTION");
	functionIdentifier();
	optionalFormalParameterList();
	type();
}

void MPParser::optionalFormalParameterList(void)
{
	switch(lookAheadNumber)
	{
		case 35://left parens
			match("MP_LPAREN");
			formalParameterSection();
			formalParameterSectionTail();
			match("MP_RPAREN");
			break;
		case 47://colon
			match("MP_EMPTY");
			break;
		case 34://semi-colon
			match("MP_EMPTY");
			break;
		case 13: //integer
			match("MP_EMPTY");
			break;
		case 9: //float
			match("MP_EMPTY");
			break;
		case 49: //boolean 
			match("MP_EMPTY");
			break;
		case 8: //fixed
			match("MP_EMPTY");
			break;
		case 54: //string
			match("MP_EMPTY");
			break;
		default:
			//cout << "error in optionalFormalParameterList line 489" << endl;
			error();
			break;
	}
}

void MPParser::formalParameterSectionTail(void)
{
	switch(lookAheadNumber)
	{
		case 34://semicolon
			match("MP_SCOLON");
			formalParameterSection();
			formalParameterSectionTail();
			break;
		case 36://rparen
			match("MP_EMPTY");
			break;
		default:
			//cout << "error in formalParameterSectionTail line 512" << endl;
			error();
			break;
	}
}

void MPParser::formalParameterSection(void)
{
	switch(lookAheadNumber)
	{
		case 27://identifier
			valueParameterSection();
			break;
		case 24://var
			variableParameterSection();
			break;
		default:
			//cout << "error in formalParameterSection line 531" << endl;
			error();
			break;
	}
}

void MPParser::valueParameterSection(void)
{
	identifierList();
	match("MP_COLON");
	type();
}

void MPParser::variableParameterSection(void)
{
	match("MP_VAR");
	identifierList();
	match("MP_COLON");
	type();
}
  
//statementPart, compoundstatement 
//statementSequence, statementTail


void MPParser::statementPart(void)
{
	compoundStatement();
}

void MPParser::compoundStatement(void)
{
	match("MP_BEGIN");
	clearExpRec();
	statementSequence();
	match("MP_END");
}

void MPParser::statementSequence(void)
{
	statement();
	statementTail();
}

void MPParser::statementTail(void)
{
	switch(lookAheadNumber)
	{
		case 34:// semicolon
			match("MP_SCOLON");
			statement();
			statementTail();
			break;
		case 7://end
			match("MP_EMPTY");
			break;
		case 23://until
			match("MP_EMPTY");
			break;
		case 6://else
			match("MP_EMPTY");
			break;
		default: //error case
			//cout << "error in statementTail line 585" << endl;
			error();
			break;
	}
}

//methods: statement,emptyStatement, readStatement, readParameterTail, readParameter
//writeStatement, writeParameterTail, writeParameter, assignmentStatement 
//ifstatement, optionalElsePart,RepeatStatement, whileStatement, forStatement
//controlVariable, initialValue, stepValue, finalValue, procedureStatement,
//optionalActualParameterList, actualParameterTail, actualParameter
void MPParser::statement(void)
{
	switch(lookAheadNumber)
	{
		case 34://semi-colon
			emptyStatement();
			break;
		case 6://else
			emptyStatement();
			break;
		case 7://end
			emptyStatement();
			break;
		case 23://until
			emptyStatement();
			break;
		case 2://begin
			compoundStatement();
			break;			
		case 19://read
			readStatement();
			break;		
		case 26: //write
			writeStatement();
			break;	
		case 50: //writeln
			writeStatement();
			break;	
		case 27:// identifier
			assignmentOrProcedureStatement();
			break;			
		case 12:// if statement
			ifStatement();
			break;	
		case 25: //while statement
			whileStatement();
			break;
		case 20: // repeat
			repeatStatement();
			break;
		case 10: // for
			forStatement();
			break;
		case 17: // procedure
			procedureStatement();
			break;
		default: //error case
			//cout << "error in statement line 609" << endl;
			error();
			break;
	}
}

void MPParser::emptyStatement(void)
{
	match("MP_EMPTY");
}

void MPParser::readStatement(void)
{
	match("MP_READ");
	match("MP_LPAREN");
	readParameter();
	readParameterTail();
	match("MP_RPAREN");
}

void MPParser::readParameterTail(void)
{
	switch(lookAheadNumber)
	{
		case 33: //comma
			match("MP_COMMA");
			readParameter();
			readParameterTail();
			break;
		case 36: //right paren
			match("MP_EMPTY");
			break;
		default: //error case
			//cout << "error in readParameterTail line 676" << endl;
			error();
			break;
	}
}

void MPParser::readParameter(void)
{
	variableIdentifier();
	if(!err && !an.getErr()){an.genRead(identifierQueue.front());}
	clearIDQ();
}

void MPParser::writeStatement(void)
{
	switch(lookAheadNumber)
	{
		case 26://write
			match("MP_WRITE");
			match("MP_LPAREN");
			writeParameter();
			writeParameterTail();
			match("MP_RPAREN");
			break;
		case 50: //writeln
			match("MP_WRITELN");
			match("MP_LPAREN");
			writeParameter();
			writeParameterTail();
			if(!err && !an.getErr()){an.genWrtLn();}
			match("MP_RPAREN");
			break;
		default: //error case
			//cout << "error in writeStatement line 700" << endl;
			error();
			break;
	}
}

void MPParser::writeParameterTail(void)
{
	switch(lookAheadNumber)
	{
		case 33: //comma
			match("MP_COMMA");
			writeParameter();
			writeParameterTail();
			break;
		case 36: //right paren
			match("MP_EMPTY");
			break;
		default: //error case
			//cout << "error in writeParameterTail line 725" << endl;
			error();
			break;
	}
}



void MPParser::writeParameter(void)
{
	ordinalExpression();
	clearIDQ();
	string tempType = getExpType();
	if(!err && !an.getErr())
	{
		an.genExp(tempType, exp_rec);
		clearExpRec();
		an.genWrt();
	}
}



void MPParser::assignmentStatement(void)
{
	switch(lookAheadNumber)
	{
		case 27://identifier
			variableIdentifier();
			match("MP_ASSIGN");
			expression();
			//Generate()
			break;
		default: //error case
			//cout << "error in assignmentStatement line 748" << endl;
			error();
			break;
	}
}

void MPParser::ifStatement(void)
{
	int elseLabel;
	int afterTrueLabel;
	match("MP_IF");
	if(!err && !an.getErr())
	{
		an.genLabel();
	}
	booleanExpression();
	match("MP_THEN");
	if(!err && !an.getErr())
	{
		elseLabel = an.ifBranch();
	}
	statement();
	if(!err && !an.getErr())
	{
		afterTrueLabel = an.branchAfterIfTrue();
		an.genLabel(elseLabel);
	}
	optionalElsePart();
	if(!err && !an.getErr())
	{
		an.genLabel(afterTrueLabel);
	}
	clearExpRec();
}

void MPParser::optionalElsePart(void)
{
	switch(lookAheadNumber)
	{	
		case 6: //else
			match("MP_ELSE");
			statement();
			break;
		case 34://semi-colon
			match("MP_EMPTY");
			break;
		case 7: //end
			match("MP_EMPTY");
			break;
		default: //error case
			//cout << "error in optionalElsePart line 773" << endl;
			error();
			break;	
	}
}

void MPParser::repeatStatement(void)
{	

	int label, currentLabel;
	if(!err && !an.getErr())
	{
		currentLabel = an.genLabel();
	}
	match("MP_REPEAT");
	statementSequence();
	match("MP_UNTIL");
	booleanExpression();
	if(!err && !an.getErr())
	{
		label = an.repUnLoopBranch(currentLabel);
		an.genLabel(label);
	}
}

void MPParser::whileStatement(void)
{
	int label;
	int currentLabel;
	if(!err && !an.getErr())
	{
		currentLabel =	an.genLabel();
	}
	match("MP_WHILE");
	booleanExpression();
	if(!err && !an.getErr()){label = an.loopCheckFirstBranch(currentLabel);}
	match("MP_DO");
	statement();
	if(!err && !an.getErr())
	{
		an.genEndLoop(currentLabel);
		an.genLabel(label);
	}
}

void MPParser::forStatement(void)
{
	int label = 0;
	int currentLabel;
	string incReg;
	match("MP_FOR");
	incReg = controlVariable();
	match("MP_ASSIGN");
	initialValue();
	if(!err && !an.getErr()) 
	{
		
		an.genAssign(id_rec, exp_rec);
		clearExpRec();	
		currentLabel = an.genLabel();
	}
	stepValue();
	finalValue();
	if(!err && !an.getErr())
	{
		string type = getExpType();
	 	an.pushControl(incReg);
		an.genExp(type, exp_rec);
		an.genForCmp(type);
		label = an.loopCheckFirstBranch(currentLabel);
		clearExpRec();
	}
	match("MP_DO");
	statement();
	if(!err && !an.getErr()){	
		an.genForInc(incReg);
		an.genEndLoop(currentLabel);
		an.genLabel(label);
	}
}

string  MPParser::controlVariable(void)
{
	id_rec.clear();	
	id_rec = newTokenData.lexeme;
	variableIdentifier();
	return id_rec;
}

void MPParser::initialValue(void)
{
	clearExpRec();
	ordinalExpression();
}

void MPParser::stepValue(void)
{
	switch(lookAheadNumber)
	{
		case 22:// to
			match("MP_TO");
			if(!err && !an.getErr()){an.setForInc("up");}
			break;
		case 5: //downto
			match("MP_DOWNTO");
			if(!err && !an.getErr()){an.setForInc("down");}
			break;
		default: //error case
			//cout << "error in stepValue line 834" << endl;
			error();
			break;
	}
}

void MPParser::finalValue(void)
{
	ordinalExpression();
}

void MPParser::procedureStatement(void)
{
	procedureIdentifier();
	optionalActualParameterList();
}

void MPParser::procedureStatementTail(void)
{
	optionalActualParameterList();
}

void MPParser::assignmentStatementTail(void)
{
	match("MP_ASSIGN");
	expression();	
	if(!err && !an.getErr()){an.genAssign(id_rec, exp_rec);}
	clearExpRec();
	id_rec.clear();
}
void MPParser::assignmentOrProcedureStatement(void)
{
	id_rec = newTokenData.lexeme;	
	match("MP_IDENTIFIER");
	switch(lookAheadNumber)
	{
		case 35://left paren
			procedureStatementTail();
			break;
		case 34://semi-colon
			procedureStatementTail();
			break;
		case 43://assign
			assignmentStatementTail();
			break;
		default:
			//cout << "error in assignmentOrProcedureStatement line 873" << endl;
			error();
			break;
	}
}
void MPParser::optionalActualParameterList(void)
{
	switch(lookAheadNumber)
	{
		case 35://left paren
			match("MP_LPAREN");
			actualParameter();
			actualParameterTail();
			match("MP_RPAREN");
			break;
		case 34: //semi-colon
			match("MP_EMPTY");
			break;
		case  46://multiply operator *
			match("MP_EMPTY");
			break;
		case  1://and
			match("MP_EMPTY");
			break;
		case  3://div
			match("MP_EMPTY");
			break;
		case  7://end
			match("MP_EMPTY");
			break;
		case  14://mod
			match("MP_EMPTY");
			break;
		default: //error case
			//cout << "error in optionalActualParameterList line 894" << endl;
			error();
			break;
	}
}

void MPParser::actualParameterTail(void)
{
	switch(lookAheadNumber)
	{
		case 33: //comma
			match("MP_COMMA");
			actualParameter();
			actualParameterTail();
			break;
		case 36: //right parens
			match("MP_EMPTY");
			break;
		default: //error case
			//cout << "error in actualParameterTail line 929" << endl;
			error();
			break; 
	}		
}

void MPParser::actualParameter(void)
{
	ordinalExpression();
}



//methods: expression, optionalRelationalPart, relationalOperator
//simpleExpression, termTail, optionalSign, addingOperator, term, 
//factorTail, multiplyingOperator, factor
void MPParser::expression(void)
{
	if(lookAheadNumber == 31)
	{
		simpleExpression();
	}
	else
	{
		simpleExpression();
		optionalRelationalPart();
	}
}

void MPParser::optionalRelationalPart(void)
{
	switch(lookAheadNumber)
	{
		case 37://=
			relationalOperator();
			simpleExpression();
			break;
		case 38://>
			relationalOperator();
			simpleExpression();
			break;
		case 39://>=
			relationalOperator();
			simpleExpression();
			break;
		case 40://<
			relationalOperator();
			simpleExpression();
			break;
		case 41://<=
			relationalOperator();
			simpleExpression();
			break;
		case 42://<>
			relationalOperator();
			simpleExpression();
			break;
		case 35://left paren
			match("MP_EMPTY");
			break;
		case 44://+
			match("MP_EMPTY");
			break;
		case 45://-
			match("MP_EMPTY");
			break;
		case 15://not
			match("MP_EMPTY");
			break;
		case 27://identifier
			match("MP_EMPTY");
			break;
		case 28://int_lit
			match("MP_EMPTY");
			break;
		case 30://float_lit
			match("MP_EMPTY");
			break;
		case 29://fixed_lit
			match("MP_EMPTY");
			break;
		case 34://semicolon
			match("MP_EMPTY");
			break;
		case 36://right paren
			match("MP_EMPTY");
			break;
		case 4: //do
			match("MP_EMPTY");
			break;
		case 22: //to
			match("MP_EMPTY");
			break;
		case 5://downto
			match("MP_EMPTY");
			break;
		case 21: //then
			match("MP_EMPTY");
			break;
		case 33://comma
			match("MP_EMPTY");
			break;
		case 7://end
			match("MP_EMPTY");
			break;
		case 6://else
			match("MP_EMPTY");
			break;
		default:
			//cout << "error in optionalRelationalPart line 971" << endl;
			error();
			break;
	}
}

void MPParser::relationalOperator(void)
{
	switch(lookAheadNumber)
	{
		case 37://=	
			exp_rec.push(newTokenData);
			match("MP_EQUAL");
			break;
		case 38://>
			exp_rec.push(newTokenData);
			match("MP_GTHAN");
			break;
		case 39://>=
			exp_rec.push(newTokenData);
			match("MP_GEQUAL");
			break;
		case 40://<
			exp_rec.push(newTokenData);
			match("MP_LTHAN");
			break;
		case 41://<=
			exp_rec.push(newTokenData);
			match("MP_LEQUAL");
			break;
		case 42://<>
			exp_rec.push(newTokenData);
			match("MP_NEQUAL");
			break;
		default:
			//cout << "error in relationalOperator line 1036" << endl;
			error();
			break;
	}
}

void MPParser::simpleExpression(void)
{
	optionalSign();
	term();
	termTail();
}

void MPParser::termTail(void)
{
	switch(lookAheadNumber)
	{
		case 44://+
			addingOperator();
			term();
			termTail();
			break;
		case 45://-
			addingOperator();
			term();
			termTail();
			break;
		case 16://or
			addingOperator();
			term();
			termTail();
			break;
		case 36://Right paren
			match("MP_EMPTY");
			break;
		case 34://semicolon
			match("MP_EMPTY");
			break;
		case 41://<=
			match("MP_EMPTY");
			break;
		case 37://=
			match("MP_EMPTY");
			break;
		case 38://>
			match("MP_EMPTY");
			break;
		case 39://>=
			match("MP_EMPTY");
			break;
		case 40://<
			match("MP_EMPTY");
			break;
		case 42://<>
			match("MP_EMPTY");
			break;
		case 4: //do
			match("MP_EMPTY");
			break;
		case 22: //to
			match("MP_EMPTY");
			break;
		case 5://downto
			match("MP_EMPTY");
			break;
		case 21: //then
			match("MP_EMPTY");
			break;
		case 33://comma
			match("MP_EMPTY");
			break;
		case 7://end
			match("MP_EMPTY");
			break;
		case 6://else
			match("MP_EMPTY");
			break;
		default:
			//cout << "error in termTail line 1079" << endl;
			error();
			break;
	}
}

void MPParser::optionalSign(void)
{
	switch(lookAheadNumber)
	{
		case 44://+
			exp_rec.push(newTokenData);
			match("MP_PLUS");
			break;
		case 45://-
			exp_rec.push(newTokenData);
			match("MP_MINUS");
			break;
		case 27://identifier
			match("MP_EMPTY");
			break;
		case 28://int_lit
			match("MP_EMPTY");
			break;
		case 29://fixed_lit
			match("MP_EMPTY");
			break;
		case 30://float_lit
			match("MP_EMPTY");
			break;
		case 15://not
			match("MP_EMPTY");
			break;
		case 35://left paren
			match("MP_EMPTY");
			break;
		case 52://true
			match("MP_EMPTY");
			break;
		case 53://false
			match("MP_EMPTY");
			break;
		case 31://string
			match("MP_EMPTY");
			break;
		default:
			//cout << "error in optionalSign line 1130" << endl;
			error();
			break;
	}
}

void MPParser::addingOperator(void)
{
	switch(lookAheadNumber)
	{
		case 44://+
			exp_rec.push(newTokenData);
			match("MP_PLUS");
			break;
		case 45://-
			exp_rec.push(newTokenData);
			match("MP_MINUS");
			break;
		case 16://or
			exp_rec.push(newTokenData);
			match("MP_OR");
			break;
		default:
			error();
			break;
	}
}

void MPParser::term(void)
{
	factor();
	factorTail();
}

void MPParser::factorTail(void)
{
	switch(lookAheadNumber)
	{
		case 46://*
			multiplyingOperator();
			factor();
			factorTail();
			break;
		case 3://div
			multiplyingOperator();
			factor();
			factorTail();
			break;
		case 51:// divide
			multiplyingOperator();
			factor();
			factorTail();
			break;
		case 14://mod
			multiplyingOperator();
			factor();
			factorTail();
			break;
		case 1://and
			multiplyingOperator();
			factor();
			factorTail();
			break;
		case 44://+
			match("MP_EMPTY");
			break;
		case 45://-
			match("MP_EMPTY");
			break;
		case 16://or
			match("MP_EMPTY");
			break;
		case 36://right paren
			match("MP_EMPTY");
			break;
		case 37://=
			match("MP_EMPTY");
			break;
		case 40://<
			match("MP_EMPTY");
			break;
		case 38://>
			match("MP_EMPTY");
			break;
		case 41://<=
			match("MP_EMPTY");
			break;
		case 39://>=
			match("MP_EMPTY");
			break;
		case 42://<>
			match("MP_EMPTY");
			break;
		case 34:
			match("MP_EMPTY");
			break;
		case 4: //do
			match("MP_EMPTY");
			break;
		case 22: //to
			match("MP_EMPTY");
			break;
		case 5://downto
			match("MP_EMPTY");
			break;
		case 21: //then
			match("MP_EMPTY");
			break;
		case 33://comma
			match("MP_EMPTY");
			break;
		case 6://else
			match("MP_EMPTY");
			break;
		case 7://end
			match("MP_EMPTY");
			break;
		default:
			error();
			break;
	}
}

void MPParser::multiplyingOperator(void)
{
	switch(lookAheadNumber)
	{
		case 46://*
			exp_rec.push(newTokenData);
			match("MP_TIMES");
			break;
		case 3://div
			exp_rec.push(newTokenData);			
			match("MP_DIV");
			break;
		case 14://mod
			exp_rec.push(newTokenData);
			match("MP_MOD");
			break;
		case 1://and
			exp_rec.push(newTokenData);
			match("MP_AND");
			break;
		case 51://divide
			exp_rec.push(newTokenData);
			match("MP_DIVIDE");
			break;
		default:
			error();
			break;
	}
}

void MPParser::factor(void)
{
	switch(lookAheadNumber)
	{
		case 28: //int_lit
			exp_rec.push(newTokenData);
			match("MP_INTEGER_LIT");
			break;
		case 29://fixed_lit
			exp_rec.push(newTokenData);
			match("MP_FIXED_LIT");
			break;
		case 30://float_lit
			exp_rec.push(newTokenData);
			match("MP_FLOAT_LIT");
			break;
		case 15://not
			exp_rec.push(newTokenData);
			match("MP_NOT");
			factor();
			break;
		case 35://(
			exp_rec.push(newTokenData);
			match("MP_LPAREN");
			expression();
			exp_rec.push(newTokenData);
			match("MP_RPAREN");
			break;
		case 27://identifier
			exp_rec.push(newTokenData);
			variableIdentifier();
			if(lookAheadNumber == 35)
			{
				optionalActualParameterList();
			}
			break;
		case 31://string
			exp_rec.push(newTokenData);
			match("MP_STRING_LIT");
			break;
		case 52://true
			exp_rec.push(newTokenData);
			match("MP_TRUE");
			break;
		case 53://false
			exp_rec.push(newTokenData);
			match("MP_FALSE");
			break;
		default:
			//cout << "error in factor line 1281" << endl;
			error();
			break;
	}
}

//methods: ALL METHODS THAT MATCH AN IDENTIFIER
//booleanExpression, ordinalExpression, identifierList, identifierTail
void MPParser::programIdentifier(void)
{
	insertToSymbolTable(newTokenData.lexeme, "", "Program");
	match("MP_IDENTIFIER");

}

void MPParser::variableIdentifier(void)
{
	identifierQueue.push(newTokenData.lexeme);  
	match("MP_IDENTIFIER");
}

void MPParser::procedureIdentifier(void)
{
  insertToSymbolTable(newTokenData.lexeme, "", "Procedure");
  pushSymbolTable(newTokenData.lexeme, "", "Procedure");
  pushNext = true;
  match("MP_IDENTIFIER");
}

void MPParser::functionIdentifier(void)
{
  insertToSymbolTable(newTokenData.lexeme, "", "Function");
  pushSymbolTable(newTokenData.lexeme, "", "Function");
  pushNext = true;
  match("MP_IDENTIFIER");
}

void MPParser::booleanExpression(void)
{
	expression();
	if(!err && !an.getErr())
	{
		an.genExp("Boolean", exp_rec);
	}
	clearExpRec();
}

void MPParser::ordinalExpression(void)
{
	expression();
}

void MPParser::identifierList(void)
{
	identifierQueue.push(newTokenData.lexeme);
	match("MP_IDENTIFIER");
	identifierTail();
}

void MPParser::identifierTail(void)
{
	switch(lookAheadNumber)
	{
		case 33://comma
			match("MP_COMMA");
			identifierQueue.push(newTokenData.lexeme);
			match("MP_IDENTIFIER");
			identifierTail();
			break;
		case 47://colon
			match("MP_EMPTY");
			break;
		default:
			//cout << "error in identifierTail line 1368" << endl;
			error();
			break;
	}
}

MPParser::~MPParser(void)
{
}

