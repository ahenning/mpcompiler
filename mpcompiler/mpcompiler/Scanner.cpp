#include "Scanner.h"

struct tokenLexemePair
{
	string token;
	string lexeme;
	int row;
	int column;
} token_lexeme_pair;

struct resWordTable
{
	string tok;
	string lex;
};


Scanner::Scanner(void)
{

	
}

void Scanner::openFile(string fileName)
{
	ifstream file;
	file.open (fileName.c_str());
	if(file.is_open())
	{
		string data;
		
		lineCount = 0;
		columnCount = 0;
		while(!file.eof())
		{
			switch (file.peek())
			{
			case 32: //space 
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				columnCount++;
				file.get();
				data.clear();
			break;
			case 9: // Tab
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				file.get();
				columnCount = columnCount + 8;
				data.clear();
			break;
			case 39: //apostrophe
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				data.clear();
				//file.get();
				data.append(1, (char)file.get());
				columnCount++;
				while(file.peek() != 39)
				{
					data.append(1, (char)file.get());
					columnCount++;
					if(file.peek() == 39)
					{
					  file.get();
					  //data.append(1, (char)file.get());
					  columnCount++;
					  if(file.peek() == 39){
					    data.append(1, (char)file.get());
					  }
					  else
					  {
					      break;
					  }
					}
					if(file.peek() == 10 || file.peek() == 13 || file.eof()){
					    errs[errCount].errToken = "MP_RUN_STRING";
					    errs[errCount].errLexeme = "runstring ";
					    errs[errCount].errString = "There is a run on string";
					    errs[errCount].errRow = token_lexeme_pair.row;
						errs[errCount].errColumn = token_lexeme_pair.column;
						outFile << errCount << ": ERROR: " << errs[errCount].errToken << "\t" << errs[errCount].errLexeme << "\t";
						outFile << errs[errCount].errString << "\tStarting at line: " << errs[errCount].errRow << " column: " << errs[errCount].errColumn << endl;
					    errCount++;
						data.clear();
					    break;
					}
				}
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				data.clear();
			break;
			case 123: //open Bracket {
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				data.clear();
				file.get();
				columnCount++;
				while(file.peek() != 125) //125 close bracket }
				{
					file.get();
					columnCount++;
					if(file.eof() || file.peek() == 123){
					    errs[errCount].errToken = "MP_RUN_COMMENT";
					    errs[errCount].errLexeme = "runcomment ";
					    errs[errCount].errString = "There is a run on comment";
					    errs[errCount].errRow = token_lexeme_pair.row;
					    errs[errCount].errColumn = token_lexeme_pair.column;
						outFile << errCount << ": ERROR: " << errs[errCount].errToken << "\t" << errs[errCount].errLexeme << "\t";
						outFile << errs[errCount].errString << "\tStarting at line: " << errs[errCount].errRow << " column: " << errs[errCount].errColumn << endl;
					    errCount++;
					    break;
					}
					
				}
				file.get();
				columnCount++;
				data.clear();
			break;
			case 46: //period
				if(isdigit(data[0]))
				{
					data.append(1, (char)file.get());
				}
				else
				{
					token_lexeme_pair.column = columnCount;
					token_lexeme_pair.row = lineCount;
					getToken(data);
					columnCount++;
					outFile << "MP_PERIOD" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << (char)file.get() << endl;
					data.clear();
				}
			break;
			case 44: //Commma
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				columnCount++;
				outFile << "MP_COMMA" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << (char)file.get() << endl;
				data.clear();
			break;
			case 59: //semi colon
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				columnCount++;
				outFile << "MP_SCOLON" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << (char)file.get() << endl;
				data.clear();
			break;
			case 40: //left paren
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				columnCount++;
				outFile << "MP_LPAREN" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << (char)file.get() << endl;
				data.clear();
			break;
			case 41: //Right Paren
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				columnCount++;
				outFile << "MP_RPAREN" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << (char)file.get() << endl;
				data.clear();
			break;
			case 61:  // Equals
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				columnCount++;
				outFile << "MP_EQUAL" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << (char)file.get() << endl;
				data.clear();
			break;
			case 62: //Greater Than
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				data.append(1, (char)file.get());
				if(file.peek() == 61)
				{
					data.append(1, (char)file.get());
					columnCount++;
					outFile << "MP_GEQUAL" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << data << endl;
				}
				else
				{
					columnCount++;
					outFile << "MP_GTHAN" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << data << endl;
				}
				data.clear();
			break;
			case 60: //less than
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				data.append(1, (char)file.get());
				if(file.peek() == 61)
				{
					data.append(1, (char)file.get());
					columnCount++;
					outFile << "MP_LEQUAL" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << data << endl;
				}
				else if(file.peek() ==  62)
				{
					data.append(1, (char)file.get());
					columnCount++;
					outFile << "MP_NEQUAL" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << data << endl;
				}
				else
				{
					columnCount++;
					outFile << "MP_LTHAN" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << data << endl;
				}
				data.clear();
			break;
			case 43: //Plus
				if(data[data.size() - 1] == 101 || data[data.size() - 1] == 69)
				{
					data.append(1, (char)file.get());
				}
				else
				{
					token_lexeme_pair.column = columnCount;
					token_lexeme_pair.row = lineCount;
					getToken(data);
					columnCount++;
					outFile << "MP_PLUS" << "\t\t" << lineCount << "\t" << columnCount - 1 << "\t" << (char)file.get() << endl;
					data.clear();
				}
			break;
			case 45: //Minus
			if(data[data.size() - 1] == 101 || data[data.size() - 1] == 69)
				{
					data.append(1, (char)file.get());
				}
				else
				{
					token_lexeme_pair.column = columnCount;
					token_lexeme_pair.row = lineCount;
					getToken(data);
					columnCount++;
					outFile << "MP_MINUS" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << (char)file.get() << endl;
					data.clear();
				}
			break;
			case 42: //Multiply
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				columnCount++;
				outFile << "MP_TIMES" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << (char)file.get() << endl;
				data.clear();
			break;
			case 47://divide
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				columnCount++;
				outFile << "MP_DIVIDE" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << (char)file.get() << endl;
				data.clear();
				break;
			case 58: //Colon
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				data.clear();
				data.append(1, (char)file.get());
				if(file.peek() == 61)
				{
					data.append(1, (char)file.get());
					columnCount++;
					outFile << "MP_ASSIGN" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << data << endl;
				}
				else
				{
					columnCount++;
					outFile << "MP_COLON" << "\t" << lineCount << "\t" << columnCount - 1 << "\t" << data << endl;
				}
				data.clear();
			break;
			case 10: //end of line
				token_lexeme_pair.column = columnCount;
				token_lexeme_pair.row = lineCount;
				getToken(data);
				file.get();
				lineCount ++;
				columnCount = 0;
				data.clear();
			break;
			case 13: //end of line                                                                                                                        
			  token_lexeme_pair.column = columnCount;
			  token_lexeme_pair.row = lineCount;
			  getToken(data);
			  file.get();
			  columnCount = 0;
			  data.clear();
			break;
			default:
				
				if(file.eof())
				{
					token_lexeme_pair.column = columnCount;
					token_lexeme_pair.row = lineCount;
					getToken(data);
				}
				data.append(1, (char)file.get());
				columnCount++;
			break;
			}
		}
	}
	else
	{
	        outFile << "404 - File Not Found!" << endl;
	}

}


void Scanner::getToken(string currentToken)
{
	//outFile << data;
	//char numbers[] = {'0','1','2','3','4','5','6','7','8','9'}; //char array of numbers to determine if our token is a NUMBER
	if(currentToken[0] == '_')
	{
		identifierToken(currentToken);
	}
	else if(currentToken[0] == 0 || (int)currentToken[0] == -1 || (int)currentToken[0] == 9)
	{
		//DO NOTHING
	}
	else if(isdigit(currentToken[0]))
	{
		numbersToken(currentToken);
	}
	else if((int)currentToken[0] == 39)
	{
	        stringLiteralToken(currentToken);
	}
	//if the possible token does not start with an _ or NUMBER it must start with a letter.
	else
	{
		if(isalpha(currentToken[0]))
		{
	        identifierToken(currentToken);
		}
		else
		{
			errs[errCount].errToken = "MP_ERROR";
			errs[errCount].errLexeme = currentToken[0];
			errs[errCount].errString = "invalid starting character found";
			errs[errCount].errRow = token_lexeme_pair.row;
			errs[errCount].errColumn = token_lexeme_pair.column;
			outFile << errCount << ": ERROR: " << errs[errCount].errToken << "\t" << errs[errCount].errLexeme << "\t";
			outFile << errs[errCount].errString << "\tStarting at line: " << errs[errCount].errRow << " column: " << errs[errCount].errColumn << endl;
			errCount++;
		}
	}
	
}

void Scanner::identifierToken(string currentToken)
{
	
	token_lexeme_pair.token = "MP_IDENTIFIER";
	string temp;
	string isResWord;
	int tokensize = currentToken.size();
	if(currentToken[tokensize - 1] == '_')
	{
		errs[errCount].errToken = "MP_ERROR";
		errs[errCount].errLexeme = currentToken;
		errs[errCount].errString = "Underscore at the end of an identifier";
		errs[errCount].errRow = token_lexeme_pair.row;
		errs[errCount].errColumn = token_lexeme_pair.column;
		outFile << errCount << ": ERROR: " << errs[errCount].errToken << "\t" << errs[errCount].errLexeme << "\t";
		outFile << errs[errCount].errString << "\tStarting at line: " << errs[errCount].errRow << " column: " << errs[errCount].errColumn << endl;
		errCount++;
	}
	else
	{
		for(int i = 0; i < tokensize; i++)
		{
			if(currentToken[i] == '_' && currentToken[i+1] == '_')
			{
				errs[errCount].errToken = "MP_ERROR";
				errs[errCount].errLexeme = currentToken;
				errs[errCount].errString = "Multiple underscores found in a row";
				errs[errCount].errRow = token_lexeme_pair.row;
				errs[errCount].errColumn = token_lexeme_pair.column;
				outFile << errCount << ": ERROR: " << errs[errCount].errToken << "\t" << errs[errCount].errLexeme << "\t";
				outFile << errs[errCount].errString << "\tStarting at line: " << errs[errCount].errRow << " column: " << errs[errCount].errColumn << endl;
				errCount++;
				break;
			}
			temp.append(1, currentToken[i]);
			//token_lexeme_pair.lexeme.append(1, currentToken.at(i));
		}
		token_lexeme_pair.lexeme = temp;
		//Reserved word checker Fo Shizzle!
		isResWord = reservedWordCheck(temp);
	
		if(isResWord != "")
		{
				
			token_lexeme_pair.token = isResWord;
		}
	
		if(token_lexeme_pair.token.size() < 8)
		{
		outFile << token_lexeme_pair.token << "\t\t" << token_lexeme_pair.row << "\t" << token_lexeme_pair.column - temp.size() << "\t" << token_lexeme_pair.lexeme << endl;
		}
		else
		{
		outFile << token_lexeme_pair.token << "\t" << token_lexeme_pair.row << "\t" << token_lexeme_pair.column - temp.size() << "\t" << token_lexeme_pair.lexeme << endl;
		}
	}
}

void Scanner::numbersToken(string currentToken)
{
	token_lexeme_pair.token = "MP_INTEGER_LIT"; //by default, our number will be a string literal
	string temp;
	int tokensize = currentToken.size();
	bool foundError = false;
	for(int i = 0; i < tokensize; i++)
	{
	
		if(currentToken[i] == '.')
		{
		        //outFile << currentToken[i] << " " << i << endl;
			temp.append(1, currentToken[i]);
			//token_lexeme_pair.lexeme.append(1, currentToken.at(i));
			token_lexeme_pair.token = "MP_FIXED_LIT";
		}
		
		else if(currentToken[i] == 'e' || currentToken[i] == 'E' || currentToken[i] == '+' || currentToken[i] == '-')
		{
			temp.append(1, currentToken[i]);
			//token_lexeme_pair.lexeme.append(1, currentToken.at(i));
			token_lexeme_pair.token = "MP_FLOAT_LIT";
		}
		else if(!isdigit(currentToken[i]))
		{
			//error check other possible characters here
			temp.append(1, currentToken[i]);
			//token_lexeme_pair.lexeme.append(1, currentToken.at(i));
			foundError = true;
			
		}
		else
		{
		  temp.append(1, currentToken[i]);
		}
		
		
	}
	if(foundError){
	  errs[errCount].errToken = "MP_ID_ERROR";
	  errs[errCount].errLexeme = "identifier or number error";
	  errs[errCount].errString = "An identifier cannot start with a number or a number token cannot contain nondigit characters other than 'e','E', or '.'";
	  errs[errCount].errRow = token_lexeme_pair.row;
	  errs[errCount].errColumn = token_lexeme_pair.column - temp.size();
	  outFile << errCount << ": ERROR: " << errs[errCount].errToken << "\t" << errs[errCount].errLexeme << "\t";
	  outFile << errs[errCount].errString << "\tStarting at line: " << errs[errCount].errRow << " column: " << errs[errCount].errColumn << endl;
	  errCount++;
	  foundError = true;
	}
	else
	{
		token_lexeme_pair.lexeme = temp;
		outFile << token_lexeme_pair.token << "\t" << token_lexeme_pair.row << "\t" << token_lexeme_pair.column - temp.size() << "\t" << token_lexeme_pair.lexeme << endl;
	}
}

void Scanner::stringLiteralToken(string currentToken)
{
	token_lexeme_pair.token = "MP_STRING_LIT";
	string temp;
	int tokensize = currentToken.size();
	for(int i = 1; i < tokensize; i++)
	{
		/*if(currentToken.at(i) != EOL)
		{*/
		temp.append(1, currentToken[i]);
		//token_lexeme_pair.lexeme.append(1, currentToken.at(i));
		//}
	}
	token_lexeme_pair.lexeme = temp;
	outFile << token_lexeme_pair.token << "\t" << token_lexeme_pair.row << "\t" << token_lexeme_pair.column - temp.size() << "\t" << token_lexeme_pair.lexeme << endl;
}

string Scanner::reservedWordCheck(string lookUpString)
{
  string temp;
    for(int i = 0; i < lookUpString.size(); i++)
    {
      temp.append(1, tolower(lookUpString.at(i)));
    }
        resWordTable resWord[31];
		resWord[0].tok = "MP_AND";
		resWord[0].lex = "and";
		resWord[1].tok = "MP_BEGIN";
		resWord[1].lex = "begin";
		resWord[2].tok = "MP_BOOLEAN";
		resWord[2].lex = "boolean";
		resWord[3].tok = "MP_DIV";
		resWord[3].lex = "div";
		resWord[4].tok = "MP_DO";
		resWord[4].lex = "do";
		resWord[5].tok = "MP_DOWNTO";
		resWord[5].lex = "downto";
		resWord[6].tok = "MP_ELSE";
		resWord[6].lex = "else";
		resWord[7].tok = "MP_END";
        resWord[7].lex = "end";
		resWord[8].tok = "MP_FALSE";
		resWord[8].lex = "false";
        resWord[9].tok = "MP_FIXED";
        resWord[9].lex = "fixed";
        resWord[10].tok = "MP_FLOAT";
        resWord[10].lex = "float";
		resWord[11].tok = "MP_FOR";
		resWord[11].lex = "for";
		resWord[12].tok = "MP_FUNCTION";
        resWord[12].lex = "function";
        resWord[13].tok = "MP_IF";
        resWord[13].lex = "if";
        resWord[14].tok = "MP_INTEGER";
        resWord[14].lex = "integer";
        resWord[15].tok = "MP_MOD";
        resWord[15].lex = "mod";
        resWord[16].tok = "MP_NOT";
        resWord[16].lex = "not";
        resWord[17].tok = "MP_OR";
        resWord[17].lex = "or";
        resWord[18].tok = "MP_PROCEDURE";
        resWord[18].lex = "procedure";
        resWord[19].tok = "MP_PROGRAM";
        resWord[19].lex = "program";
		resWord[20].tok = "MP_READ";
        resWord[20].lex = "read";
        resWord[21].tok = "MP_REPEAT";
        resWord[21].lex = "repeat";
		resWord[22].tok = "MP_STRING";
		resWord[22].lex = "string";
        resWord[23].tok = "MP_THEN";
        resWord[23].lex = "then";
        resWord[24].tok = "MP_TO";
        resWord[24].lex = "to";
		resWord[25].tok = "MP_TRUE";
		resWord[25].lex = "true";
        resWord[26].tok = "MP_UNTIL";
        resWord[26].lex = "until";
        resWord[27].tok = "MP_VAR";
        resWord[27].lex = "var";
        resWord[28].tok = "MP_WHILE";
        resWord[28].lex = "while";
        resWord[29].tok = "MP_WRITE";
        resWord[29].lex = "write";
		resWord[30].tok = "MP_WRITELN";
		resWord[30].lex = "writeln";

	int min = 0;
	int max = 30;
	int mid;
	int index = -1;

	while (max >= min){
		mid = min + ((max - min) / 2);
		//compNum = compString(lookUpString, resWord[mid].lex);
		
		if(temp.compare(resWord[mid].lex) < 0){
			max = mid - 1;
		}else if(temp.compare(resWord[mid].lex) > 0){
			min = mid + 1;
		}else{
			return resWord[mid].tok;
		}				
	}
	
	return "";

}



Scanner::~Scanner(void)
{
}

