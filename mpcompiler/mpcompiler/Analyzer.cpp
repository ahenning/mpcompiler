#include "Analyzer.h"

Analyzer::Analyzer(void)
{	
	analysisRoot = new symbolTable;
	forInc = "default";
}

//obtains the current symbol table for use
void Analyzer::assignTable(symbolTable *currentTable)
{
	analysisRoot = currentTable;
	regNum = 0;
	labelCounter = 0;
}

//a method to set which register is being used
void Analyzer::setRegNum(int newNum)
{
	regNum = newNum;
}

// sets up an incremeter for a for loop
void Analyzer::setForInc(string forInc)
{
	Analyzer::forInc = forInc;
}

// allows the parser to check if there has been a code generation error
bool Analyzer::getErr()
{
	return err;	
}

//This method takes care of generating code for assignment statments
void Analyzer::genAssign(string id_rec, queue<tokenLexeme> exp_rec)
{
	symbolTable *tableSearch = new symbolTable;
	tableSearch = findInSymbolTable(id_rec);
	if(tableSearch != NULL) 
	{
		// look through our symbol table and finds the type of our varible to be assigned
		string type = tableSearch->type;
		//calls method to generate expressions, and finish the assign
		genExp(type, exp_rec);
		if((strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0) && hasFloat)
		{
			genCastToInt();
		}
		// places the value into the correct register for our variable
		codeFile << "POP " << tableSearch->offSet << "(D" << regNum << ")" << endl;
	}
	//error checking
	else
	{
		cout << id_rec << " may not be declared in this scope." << endl;
		err = true;
	}
}

//handlenextToNegate deals with an optional Sign
void Analyzer::handleNextToNegate(string type, tokenLexeme next)
{
	//if the next element of the expression record is not a left parentheses, then handle the next variable or lit and negate it
	if(strcmp(next.token.c_str(), "MP_LPAREN") != 0)
	{
		genVarLit(type, next);
		if((strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0) && hasFloat)
		{
			genCastToFloat();
		}
		genNeg(type);
		opStack.pop();
	}	
}

//check if the expression is boolean expression
bool Analyzer::isBoolExp(queue<tokenLexeme> exp_rec)
{
	queue<tokenLexeme> temp = exp_rec;
	while(!temp.empty())
	{
		if(isRelOp(temp.front().token) || isBoolOp(temp.front().token))
		{
			return true;	
		}
		temp.pop();
	}
	return false;
}

//a method to generate code for expressions
void Analyzer::genExp(string type, queue<tokenLexeme> exp_rec)
{
	hasFloat = false;
	//check if the expression is a boolean expression
	bool boolExp = isBoolExp(exp_rec);
	if(strcmp(type.c_str(), "Boolean") != 0 && boolExp)
	{
		type = "Boolean";
	}
	//checks to see if our expression contains a float, used for casting
	if(strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0)
	{	
		hasFloat = checkForFloat(exp_rec);
	}

	//set relational operator type
	if((strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0) && !hasFloat)
	{
		relOpType = "Integer";
	}else
	{
		relOpType = "Float";
	}

	//check if first element of the expression record is an optional sign
	if(strcmp(exp_rec.front().token.c_str(), "MP_MINUS")==0)
	{
		if(strcmp(type.c_str(), "Float") == 0 || strcmp(type.c_str(), "Fixed") == 0 || hasFloat)
		{
			codeFile << "push #0" << endl;
			genCastToFloat();
		}else
		{
			codeFile << "push #0" << endl;
		}
		opStack.push(exp_rec.front().token);
		exp_rec.pop();
		handleNextToNegate(type, exp_rec.front());
		exp_rec.pop();
	}else if(strcmp(exp_rec.front().token.c_str(), "MP_PLUS")==0)
	{
		exp_rec.pop();
	}
	
	//While there are still parts of the expression to generate, check what kind of expression needs to be generated and call the appropirate gen method
	while(!exp_rec.empty())
	{
		if(isMulOp(exp_rec.front().token) || isAddOp(exp_rec.front().token) || strcmp(exp_rec.front().token.c_str(), "MP_LPAREN") == 0)
		{
			opStack.push(exp_rec.front().token);
			exp_rec.pop();
			if(strcmp(opStack.top().c_str(), "MP_DIVIDE") == 0)
			{
				genCastToFloat();
				hasFloat = true;
			}
			if(strcmp(opStack.top().c_str(), "MP_LPAREN") == 0 && strcmp(exp_rec.front().token.c_str(), "MP_MINUS") == 0)
			{
				if(strcmp(type.c_str(), "Float") == 0 || strcmp(type.c_str(), "Fixed") == 0 || hasFloat)
				{
					codeFile << "push #0" << endl;
					genCastToFloat();
				}else
				{
					codeFile << "push #0" << endl;
				}
				opStack.push(exp_rec.front().token);
				exp_rec.pop();
				handleNextToNegate(type, exp_rec.front());
				exp_rec.pop();
			}else if(strcmp(opStack.top().c_str(), "MP_LPAREN") == 0 && strcmp(exp_rec.front().token.c_str(), "MP_PLUS") == 0)
			{
				exp_rec.pop();
			}
			
		//takes care of boolean expressions
		}else if(isBoolOp(exp_rec.front().token)) 
		{
			if(strcmp(exp_rec.front().token.c_str(), "MP_NOT") != 0)
			{			
				completeSubExpForBool(type);
			}
			opStack.push(exp_rec.front().token);
			exp_rec.pop();
		//takes care of relational expressions
		}else if( isRelOp(exp_rec.front().token))
		{
			completeSubExpForBool(type);
			opStack.push(exp_rec.front().token);
			exp_rec.pop();
		//if a right paren is encountered, it finishes the operations to the last left paren
		}else if(strcmp(exp_rec.front().token.c_str(), "MP_RPAREN") == 0)
		{
			finishOps(type, false);
			exp_rec.pop();
		//handle variables and literals
		}else
		{
			genVarLit(type, exp_rec.front());
			exp_rec.pop();

			//cast to a float if the type is integer or boolean and a float exists in the expression
			//However, if there is a float in the expression but a mod is encountered, the terms before and after the mod can't be cast to floats
			if(!exp_rec.empty())
			{
				if((strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0) && strcmp(exp_rec.front().token.c_str(), "MP_MOD") != 0
					 && hasFloat)
				{
					if(!opStack.empty())
					{
						if(strcmp(opStack.top().c_str(), "MP_MOD") != 0)
						{
							genCastToFloat();
						}
					}else
					{
						genCastToFloat();
					}	
				}
			//do the same thing if the expression record is empty
			//don't need to check if the next element on the expression record is a mod, but do need to check if the top of the opStack is a mod.
			}else
			{
				if((strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0) && hasFloat)
				{
					if(!opStack.empty())
					{
						if(strcmp(opStack.top().c_str(), "MP_MOD") != 0)
						{
							genCastToFloat();
						}
					}else
					{
						genCastToFloat();
					}
				}
			}
			
		}
		
		//after an element of the exp_rec is handled, we need to check if the op
		if(!opStack.empty())
		{
			if(!exp_rec.empty())
			{
				if(isMulOp(opStack.top()) && strcmp(exp_rec.front().token.c_str(), "MP_LPAREN") != 0)
				{
					genVarLit(type, exp_rec.front());
					exp_rec.pop();
					if((strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0)
					 && hasFloat && strcmp(opStack.top().c_str(), "MP_MOD") != 0)
					{
						genCastToFloat();
					}
					genMul(type, opStack.top());
					opStack.pop();
				}
			}
		}
	}
	
	finishOps(type, false);
	if(boolExp && hasFloat)
	{
		genCastToInt();
	}
	
}


void Analyzer::completeSubExpForBool(string type)
{
	if(!opStack.empty())
	{
		while(!opStack.empty())
		{
			if(strcmp(opStack.top().c_str(), "MP_LPAREN") == 0)
			{
				break;
			}else if(isBoolOp(opStack.top()))
			{
				genBool(opStack.top());
				opStack.pop();
				break;
			}else if(isAddOp(opStack.top()))
			{
				genAdd(type, opStack.top());
				opStack.pop();
			}else if(isMulOp(opStack.top()))
			{
				genMul(type, opStack.top());
				opStack.pop();
			}else if(isRelOp(opStack.top()))
			{
				genRelOp(relOpType, opStack.top());
				opStack.pop();
			}
		}
	}
}

void Analyzer::finishOps(string type, bool optionalSign)
{
	if(!opStack.empty())
	{
		while(!opStack.empty())
		{
			if(strcmp(opStack.top().c_str(), "MP_LPAREN") == 0)
			{
				opStack.pop();
				break;
			}else if(isBoolOp(opStack.top()))
			{
				genBool(opStack.top());
				opStack.pop();
			}else if(isAddOp(opStack.top()))
			{
				genAdd(type, opStack.top());
				opStack.pop();
			}else if(isMulOp(opStack.top()))
			{
				genMul(type, opStack.top());
				opStack.pop();
			}else if(isRelOp(opStack.top()))
			{
				genRelOp(relOpType, opStack.top());
				opStack.pop();
			}
		}
	}
	
	if(!opStack.empty())
	{
		if(isMulOp(opStack.top()))
		{
			genMul(type, opStack.top());
			opStack.pop();
		}else if(opStack.size() == 1 && optionalSign && isAddOp(opStack.top()))
		{
			genNeg(type);
		}
	}	
}

// a method to generate code for negating values and expression
void Analyzer::genNeg(string type)
{
	if((strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0) && !hasFloat)
	{
		codeFile << "negs" << endl;
		codeFile << "adds" << endl;
	}else if((strcmp(type.c_str(), "Float") == 0 || strcmp(type.c_str(), "Fixed") == 0) 
		|| ((strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0) && hasFloat))
	{
		codeFile << "negsf" << endl;
		codeFile << "addsf" << endl;
	}else	
	{
		cout << "Error: Can not negate boolean or string types." << endl;
		err = true;
	}
}

void Analyzer::genMul(string type, string op)
{
	if((strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0) && !hasFloat)
	{
		if(strcmp(op.c_str(), "MP_TIMES") == 0)
		{
			codeFile << "muls" << endl;
		}else if(strcmp(op.c_str(), "MP_DIV") == 0 || strcmp(op.c_str(), "MP_DIVIDE") == 0)
		{
			codeFile << "divs" << endl;
		}else if(strcmp(op.c_str(), "MP_MOD") == 0)
		{
			codeFile << "mods" << endl;
			
		}
	}else if(strcmp(type.c_str(), "Float") == 0 || strcmp(type.c_str(), "Fixed") == 0)
	{
		if(strcmp(op.c_str(), "MP_TIMES") == 0)
		{
			codeFile << "mulsf" << endl;
		}else if(strcmp(op.c_str(), "MP_DIV") == 0 || strcmp(op.c_str(), "MP_DIVIDE") == 0)
		{
			codeFile << "divsf" << endl;
		}else if(strcmp(op.c_str(), "MP_MOD") == 0)
		{
			cout << "Error: Cannot use MOD operator on type Float." << endl;
			err = true;
		}
	}else if((strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0) && hasFloat)
	{
		if(strcmp(op.c_str(), "MP_TIMES") == 0)
		{
			codeFile << "mulsf" << endl;
		}else if(strcmp(op.c_str(), "MP_DIV") == 0 || strcmp(op.c_str(), "MP_DIVIDE") == 0)
		{
			codeFile << "divsf" << endl;
		}else if(strcmp(op.c_str(), "MP_MOD") == 0)
		{
			codeFile << "mods" << endl;
			genCastToFloat();
			
		}
	}else
	{
		err = true;
		cout << "Error: Cannot use" << op << " operator on type String." << endl;
	}
}

void Analyzer::genAdd(string type, string op)
{
	if((strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0) && !hasFloat)
	{
		if(strcmp(op.c_str(), "MP_PLUS") == 0)
		{
			codeFile << "adds" << endl;
		}else if(strcmp(op.c_str(), "MP_MINUS") == 0)
		{
			codeFile << "negs" << endl;
			codeFile << "adds" << endl;
		}
	}else if((strcmp(type.c_str(), "Float") == 0 || strcmp(type.c_str(), "Fixed") == 0) 
		|| ((strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0) && hasFloat))
	{
		if(strcmp(op.c_str(), "MP_PLUS") == 0)
		{
			codeFile << "addsf" << endl;
		}else if(strcmp(op.c_str(), "MP_MINUS") == 0)
		{
			codeFile << "negsf" << endl;
			codeFile << "addsf" << endl;
		}
	}else 
	{
		err = true;
		cout << "Error: Cannot preform " << op << " operator on type boolean or string." << endl;
	}
}

//generates code for comparing booleans
void Analyzer::genBool(string op)
{
	if(strcmp(op.c_str(), "MP_AND") == 0)
	{
		codeFile << "ands" << endl;
	}else if(strcmp(op.c_str(), "MP_OR") == 0)
	{
		codeFile << "ors" << endl;
	}else if(strcmp(op.c_str(), "MP_NOT") == 0)
	{
		codeFile << "nots" << endl;
	}
}

//checks to see if an operation is addtion or subtraction
bool Analyzer::isAddOp(string opCheck)
{
	if(strcmp(opCheck.c_str(), "MP_PLUS") == 0 || strcmp(opCheck.c_str(), "MP_MINUS") == 0)
	{
		return true;
	}else
	{
		return false;
	}
}

//checks to see if an operation is multiplication/division
bool Analyzer::isMulOp(string opCheck)
{
	if(strcmp(opCheck.c_str(), "MP_TIMES") == 0 || strcmp(opCheck.c_str(), "MP_DIVIDE") == 0 
		|| strcmp(opCheck.c_str(), "MP_DIV") == 0 || strcmp(opCheck.c_str(), "MP_MOD") == 0)
	{
		return true;
	}else
	{
		return false;
	}
}

// checks to see if an operation is a relational operator
bool Analyzer::isRelOp(string relCheck)
{
	if(strcmp(relCheck.c_str(), "MP_EQUAL") == 0 || strcmp(relCheck.c_str(), "MP_GTHAN") == 0 
		|| strcmp(relCheck.c_str(), "MP_GEQUAL") == 0 || strcmp(relCheck.c_str(), "MP_LTHAN") == 0 
		|| strcmp(relCheck.c_str(), "MP_LEQUAL") == 0 || strcmp(relCheck.c_str(), "MP_NEQUAL") == 0)
	{
		return true;
	}else
	{
		return false;
	}	
}

// checks to see if an operator is a boolean operator
bool Analyzer::isBoolOp(string boolCheck)
{
	if(strcmp(boolCheck.c_str(), "MP_AND") == 0 || strcmp(boolCheck.c_str(), "MP_OR") == 0 
		|| strcmp(boolCheck.c_str(), "MP_NOT") == 0)
	{
		return true;
	}else
	{
		return false;
	}
}


bool Analyzer::checkForFloat(queue<tokenLexeme> exp_rec)
{
	queue<tokenLexeme> temp;
	temp = exp_rec;
	symbolTable *tableSearch = new symbolTable;
	while(!temp.empty())
	{
		if(strcmp(temp.front().token.c_str(), "MP_FLOAT_LIT") == 0 || strcmp(temp.front().token.c_str(), "MP_FIXED_LIT") == 0)
		{
			return true;
		}else if(strcmp(temp.front().token.c_str(), "MP_IDENTIFIER") == 0)
		{
			tableSearch = findInSymbolTable(temp.front().lexeme);
			if(tableSearch != NULL)
			{
				if(strcmp(tableSearch->type.c_str(), "Float") == 0 || strcmp(tableSearch->type.c_str(), "Fixed") == 0)
				{
					return true;
				}
			}
		}
		temp.pop();

	}
	return false;
}

//generates a cast to float
void Analyzer::genCastToFloat(void)
{
	codeFile << "castsf" << endl;
}

//generates a cast to an int
void Analyzer::genCastToInt(void)
{
	codeFile << "castsi" << endl;
}

void Analyzer::genVarLit(string type, tokenLexeme varLit)
{
	symbolTable *tableSearch = new symbolTable;
	if(strcmp(varLit.token.c_str(), "MP_INTEGER_LIT") == 0)
	{
		if(strcmp(type.c_str(), "Integer") == 0 || strcmp(type.c_str(), "Boolean") == 0)
		{
			codeFile << "PUSH #" << varLit.lexeme << endl;
		}else if(strcmp(type.c_str(), "Float") == 0 || strcmp(type.c_str(), "Fixed") == 0)
		{
			codeFile << "PUSH #" << varLit.lexeme << endl;
			genCastToFloat();
		}else
		{
			err = true;
			cout << "Error: Incompatible types." << endl;
		}
	}else if(strcmp(varLit.token.c_str(), "MP_FLOAT_LIT") == 0 || strcmp(varLit.token.c_str(), "MP_FIXED_LIT") == 0)
	{
		if(strcmp(type.c_str(), "String") != 0 )
		{
			codeFile << "PUSH #" << varLit.lexeme << endl;
		}else
		{
			err = true;
			cout << "Error: Incompatible types." << endl;
		}
	}else if(strcmp(varLit.token.c_str(), "MP_STRING_LIT") == 0)
	{
		if(strcmp(type.c_str(), "String") == 0)
		{
			codeFile << "PUSH #\"" << varLit.lexeme << "\"" << endl;
		}else
		{	
			err = true;
			cout << "Error: Incompatible types." << endl;
		}
	}else if(strcmp(varLit.token.c_str(), "MP_TRUE") == 0 || strcmp(varLit.token.c_str(), "MP_FALSE") == 0)
	{
			if(strcmp(varLit.token.c_str(), "MP_TRUE") == 0)
			{
				codeFile << "PUSH #1" << endl;
			}else
			{
				codeFile << "PUSH #0" << endl;
			}
	
	}else if(strcmp(varLit.token.c_str(), "MP_IDENTIFIER") == 0)
	{
		tableSearch = findInSymbolTable(varLit.lexeme);
		if(tableSearch != NULL)
		{
			if(strcmp(type.c_str(), tableSearch->type.c_str()) == 0 || strcmp(type.c_str(), "Boolean") == 0 
				|| (strcmp(type.c_str(), "Integer") == 0 && strcmp(tableSearch->type.c_str(), "Boolean") == 0))
			{
				codeFile << "PUSH " << tableSearch->offSet << "(D" << regNum << ")" << endl;
			}else if(strcmp(type.c_str(), "Float") == 0 || strcmp(type.c_str(), "Fixed") == 0
				&& (strcmp(tableSearch->type.c_str(), "Integer") == 0 || strcmp(tableSearch->type.c_str(), "Boolean") == 0))
			{
				codeFile << "PUSH " << tableSearch->offSet << "(D" << regNum << ")" << endl;
				genCastToFloat();
			}else if(strcmp(type.c_str(), "Integer") == 0 
				&& (strcmp(tableSearch->type.c_str(), "Float") == 0 || strcmp(tableSearch->type.c_str(), "Fixed") == 0))
			{
				codeFile << "PUSH " << tableSearch->offSet << "(D" << regNum << ")" << endl;
			}else
			{
				err = true;
				cout << "Error: Incompatible types." << endl;
			}
		}else
		{
			err = true;
			cout << "Error: " << varLit.lexeme << " not defined in this scope." << endl;
		}
	}else
	{
		err = true;
		cout << "Error: not of type Literal or Identifier." << endl;
	}
}

//method to generate read statements
void Analyzer::genRead(string rdId)
{
	symbolTable *tableSearch = new symbolTable;
	tableSearch = findInSymbolTable(rdId);
	
	
	if(tableSearch != NULL)
	{
		//generates the proper read command for int, float, or fixed.
		string type = tableSearch->type;
		if(type.compare("Integer")==0)
		{
			codeFile << "RD " << tableSearch->offSet << "(D" << regNum << ")" << endl;
		}else if(type.compare("Float") == 0 || type.compare("Fixed") == 0)
		{
			codeFile << "RDF " << tableSearch->offSet << "(D" << regNum << ")" << endl;
		}
		else if(type.compare("String") == 0)
		{
			codeFile << "RDS " << tableSearch->offSet << "(D" << regNum << ")" << endl;
		}
	}
	else
	{
		err = true;
		cout << "Error: " << rdId << " not defined in this scope." << endl;
	}
}

//generates a write 
void Analyzer::genWrt(void)
{
	codeFile << "WRTS" << endl;
}

//generates a writeline
void Analyzer::genWrtLn(void)
{
	codeFile << "PUSH #\" \"" << endl;
	codeFile << "WRTLNS" << endl;
}

//generates branches for if statements
int Analyzer::ifBranch()
{
	int elseLabel = labelCounter + 1;
	codeFile << "BRFS L" << elseLabel << endl;
	labelCounter++;
	return elseLabel;
}

//generates branches for the end of conditionals
int Analyzer::branchAfterIfTrue(void)
{
	int branchTo = labelCounter + 2;
	codeFile << "BR L" << branchTo << endl;
	labelCounter++;
	return branchTo;

}

// generates branches for loops 
int Analyzer::loopBranch(int loopLabel)
{	
	int exitLabel = labelCounter + 1;
	//if the loop condition is false, dont loop. so we branch to the loop exit label
	codeFile << "BRFS L" << exitLabel << endl;
	codeFile << "BR L" << loopLabel << endl;
	labelCounter = labelCounter + 1;
	return exitLabel;
}

int Analyzer::repUnLoopBranch(int loopLabel)
{
	int exitLabel = labelCounter + 1;
	//if the loop condition is false, dont loop. so we branch to the loop exit label
	codeFile << "BRFS L" << loopLabel << endl;
	codeFile << "BR L" << exitLabel << endl;
	labelCounter = labelCounter + 1;
	return exitLabel;
}

//generates branches for exiting loops
int Analyzer::loopCheckFirstBranch(int loopLabel)
{	
	int exitLabel = loopLabel + 1;
	//if the loop condition is false, dont loop. so we branch to the loop exit label
    codeFile << "BRFS L" << exitLabel << endl;
	
	labelCounter++;
	return exitLabel;

	
}

//generates the last branch for repeating the loop
void Analyzer::genEndLoop(int loopLabel)
{
	codeFile << "BR L" << loopLabel << endl;
}

// checks to see if we are incrementing up or down for a For loop
void Analyzer::genForCmp(string type)
{
	if(strcmp(forInc.c_str(), "up") == 0)
	{
		genRelOp(type, "MP_LEQUAL");
	}else
	{
		genRelOp(type, "MP_GEQUAL");
	}
}

// generates For loop code
void Analyzer::genForInc(string reg)
{
        symbolTable *tableSearch = new symbolTable;
        tableSearch = findInSymbolTable(reg);
        if(tableSearch != NULL)
        {
                // if we are incremeting our counter up, add to the counter
		if(strcmp(forInc.c_str(), "up") == 0)
		{
			codeFile << "PUSH #1" << endl;
			codeFile << "PUSH " << tableSearch->offSet << "(D" << regNum << ")" << endl;
			codeFile << "ADDS" << endl;
			codeFile << "POP " << tableSearch->offSet << "(D" << regNum << ")" << endl;
		}
		// if we are decrementing our counter, subtract from the counter
		else if(strcmp(forInc.c_str(), "down") == 0)
		{
			codeFile << "PUSH " << tableSearch->offSet << "(D" << regNum << ")" << endl;
			codeFile << "PUSH #1" << endl;
           		codeFile << "SUBS" << endl;
            		codeFile << "POP " << tableSearch->offSet << "(D" << regNum << ")" << endl;
		}
		else if( strcmp(forInc.c_str(), "default") == 0)
		{
			err = true;
			cout << "Error: For loop must set increment to up or down." << endl;
		}
	}
	else
	{
		err = true;
		cout << "Error: " << reg << " not defined in this scope." << endl;
	}	
}

//generates the next available label when called
int Analyzer::genLabel()
{
	labelCounter++;
	codeFile << "L" << labelCounter << ":" << endl;
	return labelCounter;	 

}

// generates the exit label for conditionals when given the start label
int Analyzer::genLabel(int exitLabel)
{
	labelCounter++;
	codeFile << "L" << exitLabel << ":" << endl;
	return exitLabel;
}

//generates pushes onto the stack
void Analyzer::pushControl(string incRec)
{
	    symbolTable *tableSearch = new symbolTable;
        tableSearch = findInSymbolTable(incRec);
		codeFile << "PUSH " << tableSearch->offSet << "(D" << regNum << ")" << endl;
		
}



// generates code for relational expressions
void Analyzer::genRelOp(string type, string op)
{
	if(strcmp("Integer", type.c_str()) == 0)
	{
		if(strcmp(op.c_str(),"MP_EQUAL") == 0)
		{
			codeFile << "cmpeqs" << endl;
		}
		else if(strcmp(op.c_str(),"MP_NEQUAL") == 0)
		{
			codeFile << "cmpnes" << endl;
		}
		else if(strcmp(op.c_str(),"MP_LTHAN") == 0)
		{
			codeFile << "cmplts" << endl;
		}
		else if(strcmp(op.c_str(),"MP_LEQUAL") == 0)
		{
			codeFile << "cmples" << endl;
		}
		else if(strcmp(op.c_str(),"MP_GTHAN") == 0)
		{
			codeFile << "cmpgts" << endl;
		}
		else if(strcmp(op.c_str(),"MP_GEQUAL") == 0)
		{
			codeFile << "cmpges" << endl;
		}
	}else if(type.compare("Fixed") == 0 || type.compare("Float") == 0)
	{
		if(strcmp(op.c_str(),"MP_EQUAL") == 0)
		{
			codeFile << "cmpeqsf" << endl;
			genCastToFloat();
		}
		else if(strcmp(op.c_str(),"MP_NEQUAL") == 0)
		{
			codeFile << "cmpnesf" << endl;
			genCastToFloat();
		}
		else if(strcmp(op.c_str(),"MP_LTHAN") == 0)
		{
			codeFile << "cmpltsf" << endl;
			genCastToFloat();
		}
		else if(strcmp(op.c_str(),"MP_LEQUAL") == 0)
		{
			codeFile << "cmplesf" << endl;
			genCastToFloat();
		}
		else if(strcmp(op.c_str(),"MP_GTHAN") == 0)
		{
			codeFile << "cmpgtsf" << endl;
			genCastToFloat();
		}
		else if(strcmp(op.c_str(),"MP_GEQUAL") == 0)
		{
			codeFile << "cmpgesf" << endl;
			genCastToFloat();
		}
	}
	else 
	{
		err = true;
		cout << "Error: Those types cannot be compared." << endl;
	}

}

// generates code to initialize variables
void Analyzer::initVars(int numVars, int regNum)
{
	codeFile << "PUSH D" << regNum << endl;
	codeFile << "MOV SP D" << regNum << endl;
	codeFile << "ADD SP #" << numVars + 1 << " SP" << endl;
}

//generates a halt command so that the program may end
void Analyzer::genHalt(void)
{
	codeFile << "HLT" << endl;
}
// searches through the symbol tables to find the current lexeme so code may be generated for it
symbolTable *Analyzer::findInSymbolTable(string lookingFor)
{
	symbolTable *tempA = new symbolTable;
	symbolTable *tempB = new symbolTable;
	tempA = analysisRoot;
	tempB = analysisRoot;
	bool found = false;
	while(!found && tempB != NULL)
	{
		while(tempA != NULL)
		{
			if(strcmp(tempA->lexeme.c_str(), lookingFor.c_str()) == 0)
			{
				found = true;
				break;
			}
			tempA = tempA->nextSymbol;
		}
		if(found)
		{
			break;
		}
		tempB = tempB->nextTable;
		tempA = tempB;
	}
	return tempA;
}



Analyzer::~Analyzer(void){
}
