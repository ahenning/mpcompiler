#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "Analyzer.h"
using namespace std;

//struct tokenLexeme
//{
//	string token;
//	string lexeme;
//	int row;
//	int column;
//};


//struct symbolTable
//{
//	string lexeme;
//	string kind;
//	string type;
//	int offSet;
//	symbolTable *nextSymbol;
//	symbolTable *nextTable;
//};


class MPParser
{
public:
	MPParser(void);
	Analyzer an;
	bool pushNext;
	int lookAheadNumber;
	symbolTable *root;
	int varInitCount;
	int regNum;
	ofstream parseFile;
	void clearExpRec(void);
	void clearIDQ(void);
	string getExpType(void);
	bool err;
	void getToken(void);
	void printQueue(void);
	void printOutSymbolTables(void);
	void insertToSymbolTable(string identifier, string type, string kind);
	void pushSymbolTable(string identifier, string type, string kind);
	void popSymbolTable();
	void error(void);
	void parseIt(void);
	void match(string tok);
	void syntaxError(string matchToken);
	int getLookaheadNumber(string find);
	// systemGoal, program, programHeading and block methods
	void systemGoal(void);
	void program(void);
	void programHeading(void);
	void block(void);
	// methods variableDeclarationPart, variableDeclarationTail
	// variableDeclaration, type
	void variableDeclarationPart(void);
	void variableDeclarationTail(void);
	void variableDeclaration(void);
	void type(void);
	//next section. methods: procedureAndFunctionDeclarationPart, procedureDeclaration functionDeclaration
	// procedureHeading, functionHeading, optionalFormalParameterList,formalParameterSectionTail, formalParameterSection,
	//valueParameterSection, variableParameterSection
	void procedureAndFunctionDeclarationPart(void);
	void procedureDeclaration(void);
	void functionDeclaration(void);
	void procedureHeading(void);
	void functionHeading(void);
	void optionalFormalParameterList(void);
	void formalParameterSectionTail(void);
	void formalParameterSection(void);
	void valueParameterSection(void);
	void variableParameterSection(void);
	//statementPart, compoundstatement 
	//statementSequence, statementTail
	void statementPart(void);
	void compoundStatement(void);
	void statementSequence(void);
	void statementTail(void);
	//methods: statement,emptyStatement, readStatement, readParameterTail, readParameter
	//writeStatement, writeParameterTail, writeParameter, assignmentStatement 
	//ifstatement, optionalElsePart,RepeatStatement, whileStatement, forStatement
	//controlVariable, initialValue, stepValue, finalValue, procedureStatement,
	//optionalActualParameterList, actualParameterTail, actualParameter
	void statement(void);
	void emptyStatement(void);
	void readStatement(void);
	void readParameterTail(void);
	void readParameter(void);
	void writeStatement(void);
	void writeParameterTail(void);
	void writeParameter(void);
	void writeLnParameterTail(void);
	void writeLnParameter(void);
	void assignmentStatement(void);
	void ifStatement(void);
	void optionalElsePart(void);
	void repeatStatement(void);
	void whileStatement(void);
	void forStatement(void);
	string controlVariable(void);
	void initialValue(void);
	void stepValue (void);
	void finalValue(void);
	void procedureStatement(void);
	void optionalActualParameterList(void);
	void actualParameterTail(void);
	void actualParameter(void);
	void assignmentOrProcedureStatement(void);
	void assignmentStatementTail(void);
	void procedureStatementTail(void);
	//methods: expression, optionalRelationalPart, relationalOperator
	//simpleExpression, termTail, optionalSign, addingOperator, term, 
	//factorTail, multiplyingOperator, factor
	void expression(void);
	void optionalRelationalPart(void);
	void relationalOperator(void);
	void simpleExpression(void);
	void termTail(void);
	void optionalSign(void);
	void addingOperator(void);
	void term(void);
	void factorTail(void);
	void multiplyingOperator(void);
	void factor(void);
	//methods: ALL METHODS THAT MATCH AN IDENTIFIER
	//booleanExpression, ordinalExpression, identifierList, identifierTail
	void programIdentifier(void);
	void variableIdentifier(void);
	void procedureIdentifier(void);
	void functionIdentifier(void);
	void booleanExpression(void);
	void ordinalExpression(void);
	void identifierList(void);
	void identifierTail(void);
	~MPParser(void);
};

