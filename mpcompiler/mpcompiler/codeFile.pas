PUSH D0
MOV SP D0
ADD SP #3 SP
PUSH #"Program: repeat_UNTIL"
WRTS
PUSH #" "
WRTLNS
PUSH #"If loop is right, you should see BONGO! 3 times"
WRTS
PUSH #" "
WRTLNS
PUSH #3
POP 1(D0)
L1:
PUSH #"BONGO!"
WRTS
PUSH #" "
WRTLNS
PUSH 1(D0)
PUSH #1
negs
adds
POP 1(D0)
PUSH #0
POP 2(D0)
L2:
PUSH 2(D0)
PUSH #5
cmples
BRFS L3
PUSH #"JONGO!"
WRTS
PUSH #" "
WRTLNS
PUSH #1
PUSH 2(D0)
ADDS
POP 2(D0)
BR L2
L3:
PUSH 1(D0)
PUSH #1
cmplts
BRFS L1
BR L5
L5:
PUSH #"Done with REPEAT"
WRTS
PUSH #" "
WRTLNS
HLT
